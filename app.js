const TimerCtrl = (function () {

    let timeForSetOrRest = 0;

    let totalTime;

    let firstCountIn = true;

    const CountInCtrl = {

        COUNT_IN_TIME_TOTAL: 4,
        countInTime: 4,
        playAudio: true,
        playVoice: true,

        countIn: function () {
            this.countInTime -= 1;

            UICtrl.changeSetOrPauseTimeInDOM(this.countInTime);
            if (this.countInTime <= 0) {
                if (this.playAudio) { letsgoSound.play(); }
                this.countInTime = this.COUNT_IN_TIME_TOTAL;
                TimerCtrl.setTimerState(TimerCtrl.timerStates.RUNNING);
                firstCountIn = false;
            } else if (this.countInTime <= 3) {
                if (this.playAudio) { tickSound.play(); }
            } else {
                if (this.playAudio) { readySound.play(); }
            }
        }
    };

    const audioFiles = function () {

        if (CountInCtrl.playVoice) {
            letsgoSound = new Audio("./audio/lets_go.mp3");
            readySound = new Audio("./audio/ready.mp3");
        } else {
            letsgoSound = new Audio("./audio/done.mp3");
            readySound = new Audio("./audio/tick.mp3");
        }

        tickSound = new Audio("./audio/tick.mp3");
        doneSound = new Audio("./audio/done.mp3");

        return {
            tickSound: tickSound,
            letsgoSound: letsgoSound,
            readySound: readySound,
            doneSound: doneSound
        }
    };

    return {

        timerState: undefined,
        timeLeftTotal: totalTime,

        timerStates: {
            PAUSED: 1,
            COUNT_IN: 2,
            RUNNING: 3,
            RESTPHASE: 4,
            DONE: 5
        },

        setTimerState: function (state) {
            this.timerState = state;
            UICtrl.changeCenterLabel();
        },

        setTotalTime: function (time) {
            totalTime = time;
            this.timeLeftTotal = time;
        },

        getTotalTime: function () {
            return totalTime;
        },

        setPlayAudio: function (playAudio) {
            CountInCtrl.playAudio = playAudio;
        },

        getPlayAudio: function () {
            return CountInCtrl.playAudio;
        },

        setPlayVoice: function (playVoice) {
            CountInCtrl.playVoice = playVoice;
        },
        getPlayVoice: function () {
            return CountInCtrl.playVoice;
        },

        setAudioFiles: function () {
            audioFiles();
        },


        setFirstCountIn: function (bool) {
            firstCountIn = bool;
        },

        startIntervalTimer: function () {
            const TimerInterval = setInterval(() => {


                if (this.timerState === TimerCtrl.timerStates.RESTPHASE) {
                    this.timeLeftTotal -= 1;
                    timeForSetOrRest -= 1;
                    UICtrl.changeSetOrPauseTimeInDOM(timeForSetOrRest)
                    if (timeForSetOrRest < 6) {
                        CountInCtrl.countInTime = timeForSetOrRest;
                        this.setTimerState(this.timerStates.COUNT_IN);
                        timeForSetOrRest = 0;
                    }
                }
                else if (this.timerState === TimerCtrl.timerStates.RUNNING) {
                    this.timeLeftTotal -= 1;
                    timeForSetOrRest += 1;
                    UICtrl.changeSetOrPauseTimeInDOM(timeForSetOrRest)
                }

                else if (this.timerState === TimerCtrl.timerStates.COUNT_IN) {
                    if (!firstCountIn) {this.timeLeftTotal -= 1;}
                    CountInCtrl.countIn();
                }

                else { clearInterval(TimerInterval); } // timerState = PAUSED or DONE

                UICtrl.changeTotalTimeInDOM(this.timeLeftTotal);

                if (this.timeLeftTotal === 0) {
                    this.setTimerState(TimerCtrl.timerStates.DONE);
                    UICtrl.changeBtnText();
                    audioFiles().doneSound.play();
                }

            }, 1000);
        }
    }
})();



const ProgressCtrl = (function () {

    return {

        numSets: 0,
        numRepsTotal: 0,
        numRepsNext: 1,
        intervalUp: true,

        calcSetsAndReps: function () {
            this.numSets += 1;
            this.numRepsTotal += this.numRepsNext;
            if (this.intervalUp) { this.numRepsNext += 1; }
            else {
                if (this.numRepsNext === 1) {
                    this.numRepsNext += 1;
                    this.changeIntervalDirection(true);
                } else { this.numRepsNext -= 1; }
            }
        },

        changeIntervalDirection: function (bool_intervalUp) {
            const UISelectors = UICtrl.getSelectors();
            const intervalArrowInDOM = document.querySelector(UISelectors.intervalArrow);
            if (bool_intervalUp) {
                this.intervalUp = true;
                intervalArrowInDOM.classList.remove(UISelectors.arrowDown);
                intervalArrowInDOM.classList.add(UISelectors.arrowUp);
            }
            else {
                this.intervalUp = false;
                intervalArrowInDOM.classList.remove(UISelectors.arrowUp);
                intervalArrowInDOM.classList.add(UISelectors.arrowDown);
            }
        }
    }
})();



const UICtrl = (function () {

    const UISelectors = {
        pause_ResetBtn: '.top_display_container .top_display_children a',
        pause_ResetBtnIcon: '.top_display_container .top_display_children i',

        totalTimeLabel: '#time_total .num',

        pauseIcon: 'fa-pause',
        resetIcon: 'fa-undo',
        playIcon: 'fa-play',

        doneBtn: '#btn_done',

        minTotal: '#time_total .min',
        secTotal: '#time_total .sec',

        centerDisplayLabel: '#center_display_label',

        minSetOrRest: '#time_setOrRest .min',
        secSetOrRest: '#time_setOrRest .sec',

        setCount: '#set_count .count',
        repCountTotal: '#rep_count_total .count',
        repCountCurrent: '#rep_count_current .count',

        centerDisplayText: '.center_display .center_display_children span',

        intervalArrow: '.center_display .fas',
        intervalArrowLink: '.center_display a',
        arrowDown: 'fa-arrow-down',
        arrowUp: 'fa-arrow-up',

        settingsBtn: '#btn_settings',
        closeBtnRight: '.side_menu_right .btn_close',

        sideMenuRight: '.side_menu_right',

        totalTimeInput: '#total_time_input',

        checkBoxSound: '#checkboxSound',
        checkBoxVoice: '#checkboxVoice',

        checkBoxVoiceLabel: '.checkboxVoice'

    };

    const getMinAndSec = function (time) {
        let min = String(Math.floor(time / 60));
        let sec = String(time % 60);
        if (min.length == 1) { min = "0" + min }
        if (sec.length == 1) { sec = "0" + sec }
        return [min, sec];
    }


    return {

        getSelectors: function () {
            return UISelectors;
        },

        pauseBlink: function () {
            const pauseBlinkTimer = setInterval(() => {
                const centerDisplayText = document.querySelectorAll(UISelectors.centerDisplayText);
                if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED) {
                    centerDisplayText.forEach((element) => {
                        if (element.style.visibility === 'visible') { element.style.visibility = 'hidden'; }
                        else { element.style.visibility = 'visible'; }
                    })
                } else {
                    clearInterval(pauseBlinkTimer);
                    centerDisplayText.forEach((element) => { element.style.visibility = 'visible'; })
                }
            }, 500)
        },

        changeBtnText: function () {
            const UISelectors = UICtrl.getSelectors();
            if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING || TimerCtrl.timerState === TimerCtrl.timerStates.COUNT_IN ) {
                 document.querySelector(UISelectors.pause_ResetBtnIcon).classList.remove(UISelectors.resetIcon, UISelectors.playIcon)
                document.querySelector(UISelectors.pause_ResetBtnIcon).classList.add(UISelectors.pauseIcon)
                document.querySelector(UISelectors.doneBtn).textContent = "Done"
            }
            else if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED) {
                 document.querySelector(UISelectors.pause_ResetBtnIcon).classList.remove(UISelectors.pauseIcon, UISelectors.playIcon);
                document.querySelector(UISelectors.pause_ResetBtnIcon).classList.add(UISelectors.resetIcon);
                document.querySelector(UISelectors.doneBtn).textContent = "Start"


            } else if (TimerCtrl.timerState === TimerCtrl.timerStates.DONE) {
                 document.querySelector(UISelectors.pause_ResetBtnIcon).classList.remove(UISelectors.pauseIcon, UISelectors.playIcon);
                document.querySelector(UISelectors.pause_ResetBtnIcon).classList.add(UISelectors.resetIcon);
                document.querySelector(UISelectors.doneBtn).textContent = "Reset"
            }
        },

        changeCenterLabel: function () {
            const centerDisplayLabel = document.querySelector(UISelectors.centerDisplayLabel)
            if (TimerCtrl.timerState === TimerCtrl.timerStates.COUNT_IN) { centerDisplayLabel.textContent = "Get Ready!"; }
            else if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING) { centerDisplayLabel.textContent = "Let's Go!" }
            else if (TimerCtrl.timerState === TimerCtrl.timerStates.RESTPHASE) { centerDisplayLabel.textContent = "Rest"; }
            else if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED) { centerDisplayLabel.textContent = "Paused"; }
            else { centerDisplayLabel.textContent = "Well Done!" }
        },

        changeTotalTimeInDOM: function (timeLeftTotal) {
            timeCurrent = getMinAndSec(timeLeftTotal);
            document.querySelector(UISelectors.minTotal).textContent = timeCurrent[0];
            document.querySelector(UISelectors.secTotal).textContent = timeCurrent[1];
        },

        changeSetOrPauseTimeInDOM: function (timeForSetOrRest) {
            // prevents negative values in center display when button is pressed many times in very short time 
            if (timeForSetOrRest < 0) { timeForSetOrRest = 0; }
            timeCurrent = getMinAndSec(timeForSetOrRest);
            document.querySelector(UISelectors.minSetOrRest).textContent = timeCurrent[0];
            document.querySelector(UISelectors.secSetOrRest).textContent = timeCurrent[1];
        },

        changeCountsInDOM: function () {
            document.querySelector(UISelectors.setCount).textContent = ProgressCtrl.numSets;
            document.querySelector(UISelectors.repCountTotal).textContent = ProgressCtrl.numRepsTotal;
            document.querySelector(UISelectors.repCountCurrent).textContent = ProgressCtrl.numRepsNext;
        },

        setTotalTimeInSideMenu: function () {
            const totalTime = getMinAndSec(TimerCtrl.getTotalTime());
            document.querySelector(UISelectors.totalTimeInput).value = `${totalTime[0]}:${totalTime[1]}`
        }
    }
})();


const StorageCtrl = (function () {

    return {

        storeTotalTime: function (time, timerType) {
            // console.log(`totalTime_${timerType}`, JSON.stringify(time));
            localStorage.setItem(`totalTime_${timerType}`, JSON.stringify(time))
        },

        getTotalTimeFromStore: function (timerType) {
            let totalTime;
            if (localStorage.getItem(`totalTime_${timerType}`) === null) {
                totalTime = App.getDefaultValues().totalTimeInterval;
                console.log(totalTime);
            } else {
                totalTime = localStorage.getItem(`totalTime_${timerType}`)
            }

            return totalTime;
        },

        storePlayAudio: function (playAudio) {
            localStorage.setItem('playAudio', JSON.stringify(playAudio));

        },

        getPlayAudioFromStore: function () {
            let playAudio;
            if (localStorage.getItem('playAudio') === null) {
                playAudio = App.getDefaultValues().playAudio;
            } else {
                playAudio = JSON.parse(localStorage.getItem('playAudio'));
            }
            return playAudio;
        },

        storePlayVoice: function (playVoice) {
            localStorage.setItem('playVoice', JSON.stringify(playVoice));
        },

        getPlayVoiceFromStore: function () {
            let playVoice;
            if (localStorage.getItem('playVoice') === null) {
                playVoice = App.getDefaultValues().playVoice;
            } else {
                playVoice = JSON.parse(localStorage.getItem('playVoice'));
            }
            return playVoice;
        }


    }

})();



const ShakeCtrl = (function () {

    let shakeCount = 0;


    shake = function () {
        shakeCount += 1;

        document.querySelector('#center_display_label').textContent = `shake ${shakeCount}`


        setTimeout(() => {
            if (shakeCount > 2) {window.navigator.vibrate(200);}
            shakeCount = 0;
            document.querySelector('#center_display_label').textContent = `shake ${shakeCount}`;
        }, 1000);
    }


})();


const App = (function (UICtrl, TimerCtrl) {

    let sideMenuSize = "40%";


    const defaultValues = {
        totalTimeInterval: 30,
        playAudio: true,
        playVoice: true
    }


    function loadEventListeners() {
        const UISelectors = UICtrl.getSelectors();

        document.querySelector(UISelectors.pause_ResetBtn).addEventListener('click', onStartReset);
        document.querySelector(UISelectors.doneBtn).addEventListener('click', onDone);

        document.querySelector(UISelectors.intervalArrowLink).addEventListener('click', onIntervalArrowClick);

        document.querySelector(UISelectors.totalTimeLabel).addEventListener('click', openSlideMenu);


        window.addEventListener("keyup", KeyPressHandler);
        window.addEventListener("keydown", KeyPressHandler);

        document.querySelector(UISelectors.settingsBtn).addEventListener('click', openSlideMenu);
        document.querySelector(UISelectors.closeBtnRight).addEventListener('click', closeSlideMenu);

        document.querySelector(UISelectors.totalTimeInput).addEventListener('blur', newTotalTimeEntered);


        document.querySelector(UISelectors.checkBoxSound).addEventListener('change', checkBoxSoundChange);
        document.querySelector(UISelectors.checkBoxVoice).addEventListener('change', checkBoxVoiceChange);



        document.querySelector(UISelectors.totalTimeInput).addEventListener('keypress', function (e) {
            if (e.keyCode === 13 || e.which === 13) {
                document.querySelector(UISelectors.totalTimeInput).blur();
            }
        });


        // unfocuses all buttons after click so spacebar doesn't trigger the buttons
        document.querySelectorAll("button").forEach(function (item) {
            item.addEventListener('focus', function () {
                this.blur();
            })
        })
    }


    const KeyPressHandler = {

        delta: 350,  // time till longPress
        keyDownCount: 0,
        keyDownTime: 0,


        // with handleEvent 'this' stands for the context of the obj. How does bind() work ???   https://metafizzy.co/blog/this-in-event-listeners/
        handleEvent: function (e) {
            if (e.keyCode === 32) { this.checkDuration(e); }
        },

        checkDuration: function (e) {
            if (e.type === 'keydown') {
                this.keyDownCount += 1;
                if (this.keyDownCount < 2) {
                    this.keyDownTime = new Date();
                }
            } else {    // = keyup
                const keyUpTime = new Date();
                if (keyUpTime - this.keyDownTime > this.delta) { this.onLongPress(); }
                else { this.onShortPress(); }
                this.keyDownCount = 0;
            }
        },

        onShortPress: function () {
            if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING) {
                onDone();
            } else if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED || TimerCtrl.timerState === undefined) {
                //timerState set to undefined (when paused ) so spaceBar Press triggers countIn and not Reset
                TimerCtrl.timerState = undefined;
                onStartReset();
            } else if (TimerCtrl.timerState === TimerCtrl.timerStates.DONE) {
                onStartReset();
            }
        },

        onLongPress: function () {
            ProgressCtrl.changeIntervalDirection(!ProgressCtrl.intervalUp)
            if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING) {
                onDone();
            } else if (TimerCtrl.timerState === TimerCtrl.timerStates.RESTPHASE || TimerCtrl.timerState === TimerCtrl.timerStates.COUNT_IN) {
                if (ProgressCtrl.intervalUp) {
                    ProgressCtrl.numRepsNext = ProgressCtrl.numRepsNext + 2
                } else { ProgressCtrl.numRepsNext = ProgressCtrl.numRepsNext - 2 }
                UICtrl.changeCountsInDOM();
            }
        }
    }

    function onStartReset() {
        if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED) {
            TimerCtrl.setTimerState(undefined);
            location.reload();
        } else if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING || TimerCtrl.timerState === TimerCtrl.timerStates.COUNT_IN || TimerCtrl.timerState === TimerCtrl.timerStates.RESTPHASE) {
            TimerCtrl.setTimerState(TimerCtrl.timerStates.PAUSED);
            TimerCtrl.setFirstCountIn(true);
            UICtrl.pauseBlink()
        } else if (TimerCtrl.timerState === undefined) {
            TimerCtrl.setTimerState(TimerCtrl.timerStates.COUNT_IN);
            // TimerCtrl.startIntervalTimer();

            TimerCtrl.startIntervalTimer();

        } else if (TimerCtrl.timerState === TimerCtrl.timerStates.DONE) {
            location.reload();
        }
        UICtrl.changeBtnText();
        UICtrl.changeCenterLabel();
    }

    function onDone() {
        if (TimerCtrl.timerState === TimerCtrl.timerStates.RUNNING) {
            TimerCtrl.setTimerState(TimerCtrl.timerStates.RESTPHASE);
            ProgressCtrl.calcSetsAndReps();
            UICtrl.changeCountsInDOM();
            UICtrl.changeBtnText();
            UICtrl.changeCenterLabel();
        } else if (TimerCtrl.timerState === undefined || TimerCtrl.timerState === TimerCtrl.timerStates.DONE) {
            onStartReset();
        } else if (TimerCtrl.timerState === TimerCtrl.timerStates.PAUSED) {
            TimerCtrl.setTimerState(undefined);
            onStartReset();
        }
    }

    function openSlideMenu() {
        document.querySelector(UICtrl.getSelectors().sideMenuRight).style.width = sideMenuSize;
    }

    function closeSlideMenu() {
        document.querySelector(UICtrl.getSelectors().sideMenuRight).style.width = '0px';
    }

    function onIntervalArrowClick() {
        KeyPressHandler.onLongPress();
    }

    function inputTimeToSeconds(input) {
        return time = input[0] * 60 + input[1];
    };

    function newTotalTimeEntered() {
        const input = document.querySelector(UICtrl.getSelectors().totalTimeInput).value.split(':');
        input[0] = parseInt(input[0]);
        input[1] = parseInt(input[1]);
        // check validity
        if (input.length === 2 && input[0] <= 100 && input[1] < 60
            && Number.isInteger(input[0]) && Number.isInteger(input[1])) {
            const time = inputTimeToSeconds(input);
            TimerCtrl.setTotalTime(time);
            UICtrl.changeTotalTimeInDOM(time);
            StorageCtrl.storeTotalTime(time);
            location.reload();
        }

        UICtrl.setTotalTimeInSideMenu();
    }

    function checkBoxSoundChange(e) {
        TimerCtrl.setPlayAudio(e.target.checked);
        StorageCtrl.storePlayAudio(TimerCtrl.getPlayAudio());
        e.target.blur();

        changeCheckboxVoiceState();
    }

    function checkBoxVoiceChange(e) {

        TimerCtrl.setPlayVoice(e.target.checked);
        StorageCtrl.storePlayVoice(TimerCtrl.getPlayVoice());
        e.target.blur();

        TimerCtrl.setAudioFiles();
    }

    function changeCheckboxVoiceState() {
        const UISelectors = UICtrl.getSelectors();
        if (TimerCtrl.getPlayAudio()){
            document.querySelector(UISelectors.checkBoxVoiceLabel).style.color = '#fff'
            document.querySelector(UISelectors.checkBoxVoice).disabled = false; 
        } else {
            document.querySelector(UISelectors.checkBoxVoiceLabel).style.color = '#a4a4a4'
            document.querySelector(UISelectors.checkBoxVoice).disabled = true; 
        };
    }

    function init() {
        loadEventListeners();

        TimerCtrl.setTotalTime(StorageCtrl.getTotalTimeFromStore());

        TimerCtrl.timeLeftTotal = TimerCtrl.getTotalTime();

        UICtrl.changeTotalTimeInDOM(TimerCtrl.timeLeftTotal);
        UICtrl.setTotalTimeInSideMenu();


        const UISelectors = UICtrl.getSelectors();

        TimerCtrl.setPlayAudio(StorageCtrl.getPlayAudioFromStore());
        document.querySelector(UISelectors.checkBoxSound).checked = TimerCtrl.getPlayAudio();

        TimerCtrl.setPlayVoice(StorageCtrl.getPlayVoiceFromStore());
        document.querySelector(UISelectors.checkBoxVoice).checked = TimerCtrl.getPlayVoice();

        changeCheckboxVoiceState();

        TimerCtrl.setAudioFiles();

        // Set Up for Mobile
        if (screen.width < 750) {
            sideMenuSize = "80%";

            document.querySelector(UISelectors.centerDisplayLabel).textContent = "Press Start";
            

            // var shakeEvent = new Shake({
            //     threshold: 15,
            //     timeout: 200
            // });
            // shakeEvent.start();

            // window.addEventListener('shake', function () {
            //     ShakeCtrl.shake();
            // }, false
            // );

            //check if shake is supported or not.
            if (!("ondevicemotion" in window)) { document.querySelector('#center_display_label').textContent = "Not Supported"; }
        }
    };

    return {
        getDefaultValues: function () { return defaultValues; },

        init: init

    };
})(UICtrl, TimerCtrl);


App.init();

// document.querySelector('#center_display_label').textContent = "tüdel";
// console.log("hi???")

// window.navigator.vibrate(200);

